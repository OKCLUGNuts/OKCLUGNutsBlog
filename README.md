Blog based off of example [Jekyll] site using GitLab Pages. Read more at http://doc.gitlab.com/ee/pages/README.html

Blog located at: [OKCLUGNutsBlog]

[Jekyll]: http://jekyllrb.com/
[OKCLUGNutsBlog]: http://OKCLUGNuts.gitlab.io/OKCLUGNutsBlog/
