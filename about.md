---
layout: page
title: About
permalink: /about/
---
The Kansas City Open Source Software Enthusiasts is a group with the mission to meet people in the Kansas City area that are passionate about Open Source and to listen to their stories. 
